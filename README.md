# NextBus Reverse Proxy

A highly scalable reverse proxy service for the San Francisco's public transportation system, using the [NextBus](https://www.nextbus.com/) XML feed.
It's written in Golang, and Redis is used for both storing the stats and caching the responses. The entire setup is Dockerized.

## Setup

Just run the following on the command line:

```bash
./run.sh    # runs the container
./test.sh   # makes a few HTTP calls
```

## Configuration

The config file has to be either at ```./nextbus.toml```, or at ```./config/nextbus.toml```.
The program has the following configurable variables:

* ```nextbus.xml_feed``` : The URL for the XML feed, the default value is ```"http://webservices.nextbus.com/service/publicXMLFeed"```
* ```nextbus.slow_query_threshold``` : If the response takes longer than this many seconds, it considers the query to be slow. The default value is ```0.5```
* ```nextbus.cache_timeout``` : The HTTP response from the XML feed is cached for this many seconds, the default value is ```10```
* ```http.host``` : The address to bind the reverse proxy HTTP server to, the default value is ```"localhost:8000"```
* ```redis.network``` : The [network type](https://golang.org/pkg/net/#Dial) for the connection to the Redis server, the default value is ```"tcp"```
* ```redis.host``` : The address to connect to the Redis server, the default value is ```"localhost:6379"```
* ```redis.db``` :  The [logical database](https://redis.io/commands/select) to store all the data, the default value is ```0```
* ```redis.password``` : The password to authenticate the connection to the Redis server, the default value is ```""```
* ```redis.thread_pool_size``` : The maximum number of concurrent connections to the Redis database, the default value is ```20```
* ```logging.level``` : The [level](https://github.com/sirupsen/logrus#level-logging) of logging to the console, the default value is ```"info"```

## NextBus API

Note that the example params given here are not exhaustive, please refer to the original [XML feed](http://www.nextbus.com/xmlFeedDocs/NextBusXMLFeed.pdf) documentation for a comprehensive list of allowed params.

* ```/api/v1/stats``` : Shows stats on the slow endpoints, and the number of times each endpoint was called
* ```/api/v1/agencyList``` : List available agencies
* ```/api/v1/routeList?a=<agency_tag>``` : List routes for an agency
* ```/api/v1/routeConfig?a=<agency_tag>&r=<route_tag>``` : List details of a specific route
* ```/api/v1/predictions?a=<agency_tag>&r=<route_tag>&s=<stop_tag>``` : Obtain arrival/departure prediction for a stop
* ```/api/v1/predictionsForMultiStops?a=<agency_tag>&stops=<stop1>&stops=<stop2>``` : Obtain arrival/departure prediction for multiple stops
* ```/api/v1/schedule?a=<agency_tag>&r=<route_tag>``` : Obtain schedule information for a route
* ```/api/v1/messages?a=<agency_tag>&r=<route_tag>``` : Obtain messages active for a route
* ```/api/v1/vehicleLocations?a=<agency_tag>&r=<route_tag>&t=<epoch_time_in_msec>``` : List vehicle locations that have changed since the specified time

## Architecture

* Created an instance of [ReverseProxy](https://golang.org/pkg/net/http/httputil/#ReverseProxy), with a custom Director to ensure that the incoming HTTP requests get rewritten to the way that the NextBus API endpoint is expecting
* When we get an HTTP request, we see if the request is cached in Redis.
If yes, we fetch it, unmarshal it, and serve it.
If not, we make a request to NextBus, [marshal](https://golang.org/pkg/encoding/gob/#pkg-overview) the response, cache it with a TTL of ```nextbus.cache_timeout``` seconds, and then serve the response.
* Unfortunately, Go [doesn't allow](https://github.com/golang/go/issues/19969) marshaling a struct unless it has at least one exported field, so we had to resort to a [hack](https://stackoverflow.com/a/46862236) until the issue is solved
* Caching the responses in Redis ensures faster responses, and makes the program less prone to getting blocked by the NextBus feed
* After every call to a valid endpoint, a counter is incremented in Redis, to keep track of the number of requests that has been made to it.
This is where the stats API gets its query count data from.
* If the request takes more than ```nextbus.slow_query_threshold``` seconds, then the time taken is also stored in Redis.
This is where the stats API gets its slow query data from.
* Storing the stats in Redis ensures that all instances of this program expose the same stats, as long as they're configured to connect to the same database

## To do

* Send SIGHUP to make the program reload the config file, just like how Nginx does it
* If the existing Redis connection is broken, re-establish it instead of crashing the program
* Use a [thread pool](https://github.com/reisinger/examples-redigo/blob/master/README.md) instead of a single connection to the Redis database for maximum efficiency
* Have Nginx in the front of this program as a load balancer, and have it [automatically update](https://github.com/jwilder/nginx-proxy) its config file using [Docker-gen](https://github.com/jwilder/docker-gen) when the program is scaled using Docker Compose
