package main

import (
	log "github.com/sirupsen/logrus"
	"os"
)

func checkError(err error) {
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}
}
