#!/usr/bin/env bash

REDIS_ADDR="127.0.0.1:16379"
NEXTBUS_ADDR="127.0.0.1:8000"

announce() {
    echo -e "\n\e[32mINFO\e[0m: $1\n"
}

checkIfRunning() {
    isRunning=$(docker ps -f "name=$1"| grep "$1")
}

checkIfFailure() {
    if [[ $? -ne 0 ]] ; then
        echo -e "\n\e[31mERROR\e[0m: $1" >&2
        exit 2
    fi
}

announce "Launching the container for Redis"
checkIfRunning "nb-redis"
if [[ $? -eq 0 ]] ; then
    announce "Redis is already running"
else
    docker run -d \
        --name nb-redis \
        -p ${REDIS_ADDR}:6379 \
        redis
    checkIfFailure "Unable to launch the container for Redis"
fi

announce "Building the container for NextBus Reverse Proxy"
docker build . -t nextbus:latest
checkIfFailure "Unable to build the container for NextBus Reverse Proxy"

announce "Launching the container for NextBus Reverse Proxy"
checkIfRunning "nextbus"
if [[ $? -eq 0 ]] ; then
    announce "NextBus Reverse Proxy is already running"
else
    docker run -it -d \
        --link nb-redis:redis \
        --mount type=bind,source="$(pwd)"/nextbus.toml,target=/go/src/nextbus_reverse_proxy/nextbus.toml \
        --name nextbus \
        -p ${NEXTBUS_ADDR}:8000 \
        nextbus
    checkIfFailure "Unable to launch the container for NextBus Reverse Proxy"
fi

announce "Tailing the logs for NextBus Reverse Proxy.."
docker logs -f nextbus
