package main

import (
	"github.com/garyburd/redigo/redis"
)

func initCache(config redisConfig) redis.Conn {
	conn, err := redis.Dial(config.network, config.host)
	checkError(err)

	if len(config.password) > 0 {
		_, err = conn.Do("AUTH", config.password)
		checkError(err)
	}
	_, err = conn.Do("SELECT", config.db)
	checkError(err)

	return conn
}
