package main

import (
	"github.com/garyburd/redigo/redis"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func main() {
	config := initConfig()
	cache := initCache(config.redis)
	proxy := initReverseProxy(config.xmlFeed, cache, config.cacheTimeout)
	defer cache.Close()

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	v1 := router.Group("/api/v1")
	{
		v1.GET("/agencyList", func(c *gin.Context) {
			proxyRequest("agencyList", c, proxy, cache, config)
		})
		v1.GET("/routeList", func(c *gin.Context) {
			proxyRequest("routeList", c, proxy, cache, config)
		})
		v1.GET("/routeConfig", func(c *gin.Context) {
			proxyRequest("routeConfig", c, proxy, cache, config)
		})
		v1.GET("/predictions", func(c *gin.Context) {
			proxyRequest("predictions", c, proxy, cache, config)
		})
		v1.GET("/predictionsForMultiStops", func(c *gin.Context) {
			proxyRequest("predictionsForMultiStops", c, proxy, cache, config)
		})
		v1.GET("/schedule", func(c *gin.Context) {
			proxyRequest("schedule", c, proxy, cache, config)
		})
		v1.GET("/messages", func(c *gin.Context) {
			proxyRequest("messages", c, proxy, cache, config)
		})
		v1.GET("/vehicleLocations", func(c *gin.Context) {
			proxyRequest("vehicleLocations", c, proxy, cache, config)
		})
		v1.GET("/stats", func(c *gin.Context) {
			slowEndpoints, err := redis.StringMap(cache.Do("HGETALL", "slow"))
			checkError(err)
			countEndpoints, err := redis.StringMap(cache.Do("HGETALL", "count"))
			checkError(err)
			c.JSON(http.StatusOK, gin.H{
				"slow_requests": slowEndpoints,
				"queries":       countEndpoints,
			})
		})
	}
	log.Debug("HTTP server bound to ", config.host)
	router.Run(config.host)
}
