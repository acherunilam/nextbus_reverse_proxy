#!/usr/bin/env bash

NEXTBUS_ADDR="127.0.0.1:8000"
OUTPUT_LIMIT=10
SLEEP_LENGTH=1

callEndpoint() {
    echo -e "\n\e[32mINFO\e[0m: GET /api/v1/$1\n"
    curl -s "http://${NEXTBUS_ADDR}/api/v1/$1" | tail -n ${OUTPUT_LIMIT}
    sleep ${SLEEP_LENGTH}
}

callEndpoint "agencyList"
callEndpoint "agencyList"
callEndpoint "agencyList"
callEndpoint "routeList?a=sf-muni"
callEndpoint "routeConfig?a=sf-muni&r=N"
callEndpoint "schedule?a=sf-muni&r=N"
callEndpoint "stats"
