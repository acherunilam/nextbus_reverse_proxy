FROM golang:latest

MAINTAINER Adithya B Cherunilam (adithya.benny@gmail.com)

COPY ./*.go /go/src/nextbus_reverse_proxy/
WORKDIR /go/src/nextbus_reverse_proxy

RUN go get .
RUN go build

ENTRYPOINT /go/src/nextbus_reverse_proxy/nextbus_reverse_proxy
EXPOSE 8000
