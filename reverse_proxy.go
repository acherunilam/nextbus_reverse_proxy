package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/garyburd/redigo/redis"
	"io"
	"io/ioutil"
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"
)

// struct without at least one exported field cannot be marshaled [https://github.com/golang/go/issues/19969]
// resorting to hack until issue is fixed [https://stackoverflow.com/a/46862236]
type ReadCloser struct {
	*bytes.Reader
	body io.ReadCloser
}

func (rc ReadCloser) Close() error {
	return nil
}

func (rc *ReadCloser) UnmarshalBinary(b []byte) error {
	rc.Reader = bytes.NewReader(b)
	return nil
}

func (rc *ReadCloser) MarshalBinary() ([]byte, error) {
	b, err := ioutil.ReadAll(rc.body)
	rc.body.Close()
	rc.Reader = bytes.NewReader(b)
	return b, err
}

// overriding the default Transport to intercept the HTTP response, cache it, and then pass it along
type cachedTransport struct {
	http.RoundTripper
	cache        redis.Conn
	cacheTimeout int
}

func initReverseProxy(target string, cache redis.Conn, cacheTimeout int) *httputil.ReverseProxy {
	u, err := url.Parse(target)
	checkError(err)
	targetQuery := u.RawQuery

	proxy := &httputil.ReverseProxy{
		Director: func(req *http.Request) {
			req.Host = u.Host
			req.URL.Scheme = u.Scheme
			req.URL.Host = u.Host
			req.URL.Path = u.Path
			if targetQuery == "" || req.URL.RawQuery == "" {
				req.URL.RawQuery = targetQuery + req.URL.RawQuery
			} else {
				req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
			}
		},
		Transport: &cachedTransport{
			cache:        cache,
			cacheTimeout: cacheTimeout,
		},
	}

	log.Debug("Reverse Proxy pointed to ", target)
	gob.Register(ReadCloser{})
	return proxy
}

func (t *cachedTransport) RoundTrip(request *http.Request) (*http.Response, error) {
	key := fmt.Sprintf("response_%s", request.RequestURI)
	value, _ := redis.String(t.cache.Do("GET", key))
	log.WithFields(log.Fields{
		"value": value,
	}).Debug("Cache")

	if value == "" {
		// cache miss
		response, err := http.DefaultTransport.RoundTrip(request)
		// FIXME: temp hack to allow marshaling of http.Response
		response.Body = &ReadCloser{body: response.Body}
		// marshal the newly fetched HTTP response
		var buffer bytes.Buffer
		enc := gob.NewEncoder(&buffer)
		gErr := enc.Encode(response)
		checkError(gErr)
		// cache the marshaled value with a timeout
		_, rErr := t.cache.Do("SETEX", key, t.cacheTimeout, buffer.String())
		checkError(rErr)
		return response, err
	} else {
		// cache hit
		response := new(http.Response)
		// unmarshal the HTTP response from the cached value
		buffer := bytes.NewBufferString(value)
		dec := gob.NewDecoder(buffer)
		gErr := dec.Decode(response)
		checkError(gErr)
		return response, nil
	}
}

func proxyRequest(
	endPoint string,
	c *gin.Context,
	proxy *httputil.ReverseProxy,
	cache redis.Conn,
	config *config,
) {
	c.Request.URL.Path = ""
	c.Request.URL.RawQuery = fmt.Sprintf("command=%s&%s", endPoint, c.Request.URL.RawQuery)
	start := time.Now()
	proxy.ServeHTTP(c.Writer, c.Request)
	elapsed := time.Since(start)
	log.Debug(fmt.Sprintf("Request took %.3f sec", elapsed.Seconds()))

	_, err := cache.Do("HINCRBY", "count", endPoint, 1)
	checkError(err)
	if elapsed.Seconds() > config.slowQueryThreshold {
		value := fmt.Sprintf("%.1fs", elapsed.Seconds())
		_, err = cache.Do("HSET", "slow", endPoint, value)
		checkError(err)
	}
}
