package main

import (
	"github.com/spf13/viper"
	log "github.com/sirupsen/logrus"
)

type redisConfig struct {
	network        string
	host           string
	db             int
	password       string
	threadPoolSize int
}

type config struct {
	xmlFeed            string
	slowQueryThreshold float64
	cacheTimeout       int
	host               string
	logLevel           log.Level
	redis              redisConfig
}

func initConfig() *config {
	viper.SetConfigName("nextbus")
	viper.AddConfigPath("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Info("Config file not found, using default values")
	}

	config := new(config)
	viper.SetDefault("nextbus.xml_feed", "http://webservices.nextbus.com/service/publicXMLFeed")
	config.xmlFeed = viper.GetString("nextbus.xml_feed")
	viper.SetDefault("nextbus.slow_query_threshold", 2)
	config.slowQueryThreshold = viper.GetFloat64("nextbus.slow_query_threshold")
	viper.SetDefault("nextbus.cache_timeout", 10)
	config.cacheTimeout = viper.GetInt("nextbus.cache_timeout")
	viper.SetDefault("http.host", "localhost:8000")
	config.host = viper.GetString("http.host")
	viper.SetDefault("logging.level", "info")
	config.logLevel, err = log.ParseLevel(viper.GetString("logging.level"))
	checkError(err)
	log.SetLevel(config.logLevel)
	log.WithFields(log.Fields{
		"xmlFeed":            config.xmlFeed,
		"slowQueryThreshold": config.slowQueryThreshold,
		"cacheTimeout":       config.cacheTimeout,
		"host":               config.host,
		"logLevel":           config.logLevel,
	}).Debug()

	viper.SetDefault("redis.network", "tcp")
	config.redis.network = viper.GetString("redis.network")
	viper.SetDefault("redis.host", "localhost:6379")
	config.redis.host = viper.GetString("redis.host")
	viper.SetDefault("redis.db", 0)
	config.redis.db = viper.GetInt("redis.db")
	viper.SetDefault("redis.password", "")
	config.redis.password = viper.GetString("redis.password")
	viper.SetDefault("redis.thread_pool_size", 20)
	config.redis.threadPoolSize = viper.GetInt("redis.thread_pool_size")
	log.WithFields(log.Fields{
		"host":             config.redis.host,
		"db":               config.redis.db,
		"password":         config.redis.password,
		"thread_pool_size": config.redis.threadPoolSize,
	}).Debug("Redis")

	return config
}
